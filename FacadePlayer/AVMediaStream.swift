//
//  TFAVMediaStream.swift
//  Player
//
//  Created by Nicholas LAU on 12/7/2018.
//

import UIKit
import AVFoundation

public class AVMediaStream: NSObject, MediaStream {
    
    public var mediaStreamType: MediaStreamType {
        get {
            return MediaStreamType.audio
        }
    }
    
    public var rawMediaStream: Any!
    
    public var isSelected: Bool = false
    
    public var name: String {
        get {
            var title: String? = nil
            let titles = AVMetadataItem.metadataItems(from: option.commonMetadata, filteredByIdentifier: .commonIdentifierTitle)
            if titles.count > 0 {
                let titlesForPreferredLanguages = AVMetadataItem.metadataItems(from: titles, filteredAndSortedAccordingToPreferredLanguages: NSLocale.preferredLanguages)
                if titlesForPreferredLanguages.count > 0 {
                    title = titlesForPreferredLanguages.first?.stringValue
                }
                
                if title == nil {
                    title = titles.first?.stringValue
                }
            }
            return title ?? ""
        }
    }
    
    public var locale: String? {
        get {
            return option.extendedLanguageTag
        }
    }
    
    private override init() {
        super.init()
    }
    
    init(withRawMetaData rawData:AVMediaSelectionOption) {
        super.init()
        rawMediaStream = rawData
    }
    
    private var option: AVMediaSelectionOption {
        get {
            return rawMediaStream as! AVMediaSelectionOption
        }
    }
}

public class AVMediaAudioStream: AVMediaStream, MediaAudioStream {
    
    public var bitRate: UInt = 0
    
}

public class AVMediaVideoStream: AVMediaStream, MediaVideoStream {
    
    public var bitRate: UInt = 0
    
    public var width: UInt = 0
    
    public var height: UInt = 0
    
}

public class AVMediaSubtitleStream: AVMediaStream, MediaSubtitleStream {
    
}

