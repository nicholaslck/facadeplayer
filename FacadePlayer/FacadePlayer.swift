//
//  Player.swift
//  Player
//
//  Created by Nicholas LAU on 11/7/2018.
//

import UIKit

public protocol FacadePlayer : AnyObject, PlaybackControl, TimeControl, PlayerLayer, MediaStreamControl {
    
    var delegate: FacadePlayerDelegate? { get set }
    
    var isPlaying: Bool { get }
    
    var isBuffering: Bool { get }
    
    var playerBaseType: FacadePlayerBaseType { get }
    
    var currentPlayableItem: Playable? { get set }
    
}

public protocol PlaybackControl {
    
    func play()
    
    func pause()
    
    func stop()
    
}

public protocol TimeControl {
    
    var currentTime: TimeInterval { get }
    
    func seekTo(time: TimeInterval)
    
    func seekTo(time: TimeInterval, withCompletionHandler:((Bool) -> Void)?)
    
}

public protocol PlayerLayer {
    
    var view: UIView { get }
    
}

public protocol MediaStreamControl {
    
    func updateAudioStreams()
    
    func updateVideoStreams()
    
    func updateSubtitleStreams()
    
}
