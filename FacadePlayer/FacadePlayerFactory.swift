//
//  PlayerFactory.swift
//  Player
//
//  Created by Nicholas LAU on 11/7/2018.
//

import UIKit

public enum FacadePlayerBaseType {
    case av
    case playready
    case widevine
}

public class FacadePlayerFactory: NSObject {
    
    public static func createPlayer(withType type: FacadePlayerBaseType) -> FacadePlayer? {
        switch type {
        case .av:
            return FacadeAVPlayer();
        case .playready:
            assertionFailure("Player for \(FacadePlayerBaseType.playready) not yet implemented")
            return nil;
        case .widevine:
            assertionFailure("Player for \(FacadePlayerBaseType.widevine) not yet implemented")
            return nil;
        }
    }
}

