//
//  AssetResourceLoaderDelegate.swift
//  TFPlayer
//
//  Created by Nicholas LAU on 18/7/2018.
//

import UIKit
import AVFoundation

fileprivate let URL_SCHEME_NAME = "skd"

public class AssetResourceLoaderDelegate: NSObject{
    
    public var assetID: String = ""
    public var fpsCertificateURL: String = ""
    public var fpsServerURL: String = ""
    public var drmToken: String = ""
    public var isDebugPrintEnabled: Bool = false
    
    private var urlSession = URLSession(configuration: .default)
    
    private func debug_print(_ msg: String) {
        if isDebugPrintEnabled { print(msg) }
    }
    
    func getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest(_ requestBytes: Data, contentIdentifierHost assetStr:String, leaseExpiryDuration expiryDuration:TimeInterval, error errorOut:inout Error?) -> Data? {
        var decodedData: Data? = nil;
        var blockError: Error? = nil;
        
        let semephore = DispatchSemaphore(value: 0)
        
        guard let url = URL(string: fpsServerURL) else {
            return nil
        }
        let request = NSMutableURLRequest(url: url)
        
        let base64Encoded = requestBytes.count > 0 ? requestBytes.base64EncodedString(options: []) : ""
        let assetID = self.assetID.count > 0 ? self.assetID : assetStr
        
        let jsonObj = ["spc": base64Encoded, "asset_id": assetID]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObj, options: [])
            
            request.httpMethod = "POST"
            request.httpBody = jsonData
            request.setValue(drmToken, forHTTPHeaderField: "tfi-drm-token")
            
            let task = urlSession.dataTask(with: request as URLRequest) { [weak self] (data, response, error) in
                guard let strongSelf = self else { semephore.signal(); return }
                if error == nil {
                    guard let response = response, let data = data else {
                        return;
                    }
                    strongSelf.debug_print("response: \(response)")
                    
                    let respStr = String(data: data, encoding: String.Encoding.utf8) ?? ""
                    
                    if let r1 = respStr.range(of: "<ckc>"),
                        let r2 = respStr.range(of: "</ckc>"),
                        !r1.isEmpty && !r2.isEmpty {
                        
                        let ckcRange = r1.upperBound..<r2.lowerBound
                        let ckc = String(respStr[ckcRange])
                        decodedData = Data.init(base64Encoded: ckc)
                        
                    }
                    else {
                        let errDomian = Bundle.main.bundleIdentifier ?? ""
                        let userInfo = [NSLocalizedDescriptionKey: "<ckc> tag not found", NSLocalizedFailureReasonErrorKey: respStr]
                        blockError = NSError(domain: errDomian, code: 1, userInfo: userInfo)
                    }
                }
                else {
                    strongSelf.debug_print("getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest")
                    blockError = error
                }
                semephore.signal()
            }
            task.resume();
            _ = semephore.wait(timeout: DispatchTime.distantFuture)
            
            errorOut = blockError
            return decodedData
        }
        catch {
            return nil
        }
    }
    
    func myGetAppCertificateData() -> Data? {
        var certificate: Data? = nil
        
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string: fpsCertificateURL)
        
        if let url = url {
            let task = urlSession .dataTask(with: url) { [weak self] (data, response, error) in
                guard let strongSelf = self else {
                    semaphore.signal()
                    return
                }
                if error == nil {
                    certificate = data
                }
                else {
                   strongSelf.debug_print("myGetAppCertificateData, error = \(String(describing: error?.localizedDescription))")
                }
                semaphore.signal()
            }
            
            task.resume()
            _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        }
        
        return certificate
    }
    
    func shouldLoadOrRenewRequestedResource(_ loadingRequest:AVAssetResourceLoadingRequest) -> Bool {
        let dataRequest = loadingRequest.dataRequest
        let url = loadingRequest.request.url
        var error: Error? = nil;
        
        if (url?.scheme ?? "") != URL_SCHEME_NAME {
            debug_print("Invalid url scheme: \(url?.scheme ?? "") for AVAssetResourceLoader, only support url scheme: \(URL_SCHEME_NAME)")
            return false
        }
        
        debug_print("shouldLoadOrRenewRequestedResource got \(loadingRequest)")
        
        let assetStr = url?.host ?? ""
        let assetId = Data.init(bytes: assetStr.cString(using: String.Encoding.utf8)!, count: assetStr.lengthOfBytes(using: String.Encoding.utf8))
        
        guard let certificate = myGetAppCertificateData() else {
            debug_print("No certificate data")
            return false;
        }
        
        var spcDataOrnil: Data?
        
        do {
            try spcDataOrnil = loadingRequest.streamingContentKeyRequestData(forApp: certificate, contentIdentifier: assetId, options: nil)
        }
        catch {
            debug_print("Obtain SPC key error.")
            return false
        }
        guard let spcData = spcDataOrnil else {
            debug_print("Obtain SPC key error. spcData is nil")
            return false
        }
        
        let expiryDuration: TimeInterval = TimeInterval(0)
        
        // Send the SPC message to the Key Server and get a CKC in reply.
        guard let ckcData = getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest(spcData, contentIdentifierHost: assetStr, leaseExpiryDuration: expiryDuration, error: &error) else {
            loadingRequest.finishLoading(with: error)
            return true
        }
        
        dataRequest?.respond(with: ckcData)
        if expiryDuration != 0.0 {
            if let infoRequest = loadingRequest.contentInformationRequest {
                infoRequest.renewalDate = Date.init(timeIntervalSinceNow: expiryDuration)
                infoRequest.contentType = "application/octet-stream"
                infoRequest.contentLength = Int64(ckcData.count)
                infoRequest.isByteRangeAccessSupported = false
            }
        }
        loadingRequest.finishLoading()
        return true
    }
}

extension AssetResourceLoaderDelegate : AVAssetResourceLoaderDelegate {
    
    /* ---------------------------------------------------------
     **
     **  resourceLoader:shouldWaitForLoadingOfRequestedResource:
     **
     **   When iOS asks the app to provide a CK, the app invokes
     **   the AVAssetResourceLoader delegate’s implementation of
     **   its -resourceLoader:shouldWaitForLoadingOfRequestedResource:
     **   method. This method provides the delegate with an instance
     **   of AVAssetResourceLoadingRequest, which accesses the
     **   underlying NSURLRequest for the requested resource together
     **   with support for responding to the request.
     **
     ** ------------------------------------------------------- */
    public func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        debug_print("shouldWaitForLoadingOfRequestedResource get \(loadingRequest)")
        return shouldLoadOrRenewRequestedResource(loadingRequest)
    }
    
    /* -----------------------------------------------------------------------------
     **
     ** resourceLoader: shouldWaitForRenewalOfRequestedResource:
     **
     ** Delegates receive this message when assistance is required of the application
     ** to renew a resource previously loaded by
     ** resourceLoader:shouldWaitForLoadingOfRequestedResource:. For example, this
     ** method is invoked to renew decryption keys that require renewal, as indicated
     ** in a response to a prior invocation of
     ** resourceLoader:shouldWaitForLoadingOfRequestedResource:. If the result is
     ** YES, the resource loader expects invocation, either subsequently or
     ** immediately, of either -[AVAssetResourceRenewalRequest finishLoading] or
     ** -[AVAssetResourceRenewalRequest finishLoadingWithError:]. If you intend to
     ** finish loading the resource after your handling of this message returns, you
     ** must retain the instance of AVAssetResourceRenewalRequest until after loading
     ** is finished. If the result is NO, the resource loader treats the loading of
     ** the resource as having failed. Note that if the delegate's implementation of
     ** -resourceLoader:shouldWaitForRenewalOfRequestedResource: returns YES without
     ** finishing the loading request immediately, it may be invoked again with
     ** another loading request before the prior request is finished; therefore in
     ** such cases the delegate should be prepared to manage multiple loading
     ** requests.
     **
     ** -------------------------------------------------------------------------- */
    public func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForRenewalOfRequestedResource renewalRequest: AVAssetResourceRenewalRequest) -> Bool {
        debug_print("shouldWaitForRenewalOfRequestedResource get \(renewalRequest)")
        return shouldLoadOrRenewRequestedResource(renewalRequest)
    }
    
}
