//
//  TFIAVPlayer.swift
//  Player
//
//  Created by Nicholas LAU on 11/7/2018.
//

import UIKit
import AVFoundation

fileprivate let assetResourceLoaderDelegateQueue = DispatchQueue(label: "fair play notify queue")

public class AVPlayerView : UIView {
    
    public var playerLayer: AVPlayerLayer? {
        didSet {
            if let oldLayer = oldValue {
                oldLayer.removeFromSuperlayer()
            }
            
            if let newLayer = playerLayer {
                layer.addSublayer(newLayer)
                playerLayer?.frame = bounds
            }
            
            layoutSubviews()
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        if let _ = layer.sublayers {
            CATransaction.begin()
            
            if let animation = layer.animation(forKey: "position") {
                CATransaction.setAnimationDuration(animation.duration)
                CATransaction.setAnimationTimingFunction(animation.timingFunction)
            } else {
                CATransaction.disableActions()
            }
            
            playerLayer?.frame = bounds
            
            CATransaction.commit()
        }
    }
}

fileprivate extension TimeInterval {
    var cmTime: CMTime {
        get {
            return CMTime(seconds: self, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        }
    }
}

public class FacadeAVPlayer: NSObject {
    
    public weak var delegate: FacadePlayerDelegate?
    
    private var avPlayer: AVPlayer = AVPlayer.init()
    
    private var _currentPlayableItem: Playable? = nil
    
    private var _view: AVPlayerView = AVPlayerView()
    
    private var timeObserver: Any?
    
    private var _isBuffering: Bool = false {
        didSet {
            if _isBuffering != oldValue {
                delegate?.mediaPlayer(self, playableItem: _currentPlayableItem, bufferingStatusDidChange: _isBuffering)
            }
        }
    }
    
    private var playAfterOnReady = false;
    
    private lazy var assetResourceLoaderDelegate = AssetResourceLoaderDelegate()
    
    public override init() {
        super.init()
        
        let layer = AVPlayerLayer.init(player: avPlayer)
        layer.videoGravity = .resizeAspect
        _view.playerLayer = layer
        
        addTimeObserver()
        addPlaybackRateObserver()
    }
    
    deinit {
        removeTimeObserver()
        removePlaybackRateObserver()
        replaceCurrentAVItem(nil)
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let playerItem = object as? AVPlayerItem {
            handleObservedValueOn(playerItem: playerItem, keyPath: keyPath, change: change)
        }
        else if let player = object as? AVPlayer {
            handleObserverValueOn(player: player, keyPath: keyPath, change: change)
        }
        else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    private func needReloadAVItem() -> Bool {
        guard let avItem = avPlayer.currentItem, let playableItem = _currentPlayableItem else {
            return true
        }
        guard let itemUrl = self.urlOfPlayerItem(avItem) else {
            return true
        }
        if itemUrl != playableItem.url {
            return true
        }
        if avItem.status == .failed {
            return true
        }
        return false
    }
    
    private func urlOfPlayerItem(_ item: AVPlayerItem) -> URL? {
        let asset = item.asset
        if asset.isKind(of: AVURLAsset.self) {
            return (asset as! AVURLAsset).url
        }
        return nil
    }
    
    private func createAVPlayerItem(fromPlayable playable: Playable?) -> AVPlayerItem? {
        
        guard let playable = playable else {
            print("no playable provided")
            return nil
        }
        
        guard let url = playable.url else {
            print("no url provided")
            return nil
        }
        
        let asset = AVURLAsset(url: url)
        
        if let fairplayProvider = playable as? FairPlayResourceProvider {
            let loaderDelegate = assetResourceLoaderDelegate
            loaderDelegate.fpsCertificateURL = fairplayProvider.fpsCertificateURL
            loaderDelegate.fpsServerURL = fairplayProvider.fpsServerURL
            loaderDelegate.drmToken = fairplayProvider.drmToken
            
            asset.resourceLoader.setDelegate(loaderDelegate, queue: assetResourceLoaderDelegateQueue)
        }
        
        let avItem = AVPlayerItem(asset: asset)
        avItem.seek(to: playable.currentTime.cmTime)
        
        return avItem
    }
    
    private func replaceCurrentAVItem(_ newItem: AVPlayerItem?) {
        if let oldItem = avPlayer.currentItem {
            removeObserver(onAVPlayerItem: oldItem)
            
            _isBuffering = false
        }
        
        if let newItem = newItem {
            addObserver(onAVPlayerItem: newItem)
        }
        
        avPlayer.replaceCurrentItem(with: newItem)
    }
}

extension FacadeAVPlayer : FacadePlayer {
    
    public var isPlaying: Bool {
        return avPlayer.rate != 0
    }
    
    public var isBuffering: Bool {
        return _isBuffering
    }
    
    public var playerBaseType: FacadePlayerBaseType {
        return FacadePlayerBaseType.av
    }
    
    public var currentPlayableItem: Playable? {
        get {
            return _currentPlayableItem
        }
        set {
            _currentPlayableItem = newValue
            replaceCurrentAVItem(createAVPlayerItem(fromPlayable: newValue))
        }
    }
    
}

// MARK: PlaybackControl
extension FacadeAVPlayer : PlaybackControl {
    
    public func play() {
        
        if self.needReloadAVItem() {
            let avplayerItem = createAVPlayerItem(fromPlayable: _currentPlayableItem)
            playAfterOnReady = avplayerItem != nil ? true : false
            replaceCurrentAVItem(avplayerItem)
            
            // will play after playerItem is ready. Go to check func handleObservedValueOn()
        }
        else {
            avPlayer.play()
        }
    }
    
    public func pause() {
        playAfterOnReady = false
        avPlayer.pause()
    }
    
    public func stop() {
        pause()
        self.currentTime = 0
        self.replaceCurrentAVItem(nil)
    }
}

// MARK: TimeControl
extension FacadeAVPlayer : TimeControl {
    
    private(set) public var currentTime: TimeInterval {
        get {
            if let item = _currentPlayableItem {
                return item.currentTime
            }
            return 0;
        }
        
        set {
            if var item = _currentPlayableItem {
                item.currentTime = newValue
                delegate?.mediaPlayer(self, playableItem: item, currentTimeDidUpdate: item.currentTime, duration: item.duration)
            }
        }
    }
    
    public func seekTo(time: TimeInterval) {
        seekTo(time: time, withCompletionHandler: nil)
    }
    
    public func seekTo(time: TimeInterval, withCompletionHandler handler: ((Bool) -> Void)?) {
        let tolerance = CMTime(seconds: 2, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        if handler == nil {
            avPlayer.seek(to: time.cmTime, toleranceBefore: tolerance, toleranceAfter: tolerance)
        }
        else {
            avPlayer.seek(to: time.cmTime, toleranceBefore: tolerance, toleranceAfter: tolerance, completionHandler: handler!)
        }
    }
}

// MARK: PlayerLayer
extension FacadeAVPlayer : PlayerLayer {
    
    public var view: UIView {
        get {
            return _view
        }
    }
}

// MARK: Observers
private extension FacadeAVPlayer {
    
    func addTimeObserver() {
        removeTimeObserver()
        timeObserver = avPlayer.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: CMTimeScale(NSEC_PER_SEC)), queue: nil){ [weak self] (currentCMTime) in
            self?.currentTime = CMTimeGetSeconds(currentCMTime)
        }
    }
    
    func removeTimeObserver() {
        if timeObserver != nil {
            avPlayer.removeTimeObserver(timeObserver!)
            timeObserver = nil
        }
    }
    
    func addPlaybackRateObserver() {
        avPlayer.addObserver(self, forKeyPath: #keyPath(AVPlayer.rate), options: [.old, .new], context: nil)
    }
    
    func removePlaybackRateObserver() {
        avPlayer.removeObserver(self, forKeyPath: #keyPath(AVPlayer.rate), context: nil)
    }
    
    func addObserver(onAVPlayerItem item: AVPlayerItem?) {
        if let item = item {
            item.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: nil)
            item.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.isPlaybackBufferEmpty), options: [.new], context: nil)
            item.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.isPlaybackLikelyToKeepUp), options: [.new], context: nil)
            item.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.isPlaybackBufferFull), options: [.new], context: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        }
    }
    
    func removeObserver(onAVPlayerItem item: AVPlayerItem?) {
        if let item = item {
            item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
            item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.isPlaybackBufferEmpty))
            item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.isPlaybackLikelyToKeepUp))
            item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.isPlaybackBufferFull))
            NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: item)
        }
    }
    
    func handleObserverValueOn(player: AVPlayer, keyPath: String?, change: [NSKeyValueChangeKey : Any]?) {
        
        switch keyPath {
        case #keyPath(AVPlayer.rate):
            if let oldValue = change?[.oldKey] as? NSNumber {
                let playing = isPlaying
                if oldValue.boolValue != playing {
                    delegate?.mediaPlayer(self, playableItem: _currentPlayableItem, playbackStatusDidChange: playing)
                }
            }
            
        default:
            return
        }
    }
    
    func handleObservedValueOn(playerItem: AVPlayerItem, keyPath: String?, change: [NSKeyValueChangeKey : Any]?) {
        
        switch keyPath {
        case #keyPath(AVPlayerItem.status):
            var status: AVPlayerItemStatus = .unknown
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItemStatus(rawValue: statusNumber.intValue)!
            }
            
            switch status {
            case .readyToPlay:
                print("player item status ready to play")
                
                guard var playableItem = _currentPlayableItem else {
                    print("no playable item")
                    return
                }
                
                // update media duration
                playableItem.duration = CMTimeGetSeconds(playerItem.duration)
                currentTime = playableItem.currentTime
                
                
                // update media metadata
                setupPlayableMetadata(from: playerItem.asset)
                
                if playableItem.selectMediaStream(index: playableItem.selectedAudioIndex ?? 0, onStreamList: playableItem.availableAudios) {
                    updateAudioStreams()
                }
                if playableItem.selectMediaStream(index: playableItem.selectedVideoIndex ?? 0, onStreamList: playableItem.availableVideos) {
                    updateVideoStreams()
                }
                if playableItem.selectMediaStream(index: playableItem.selectedSubtitleIndex ?? 0, onStreamList: playableItem.availableSubtitles) {
                    updateSubtitleStreams()
                }
                
                delegate?.mediaPlayer(self, playableItemMetadataDidReady: playableItem)
                
                if playAfterOnReady {
                    playAfterOnReady = false
                    avPlayer.play()
                }
            case .failed:
                print("player item status failed: \(playerItem.error?.localizedDescription ?? "")")
                delegate?.mediaPlayer(self, playableItem: _currentPlayableItem!, metadataError: playerItem.error)
                return
            default:
                print("player item status unknown")
                return
            }
            
        case #keyPath(AVPlayerItem.isPlaybackBufferEmpty):
            _isBuffering = true
        case #keyPath(AVPlayerItem.isPlaybackLikelyToKeepUp), #keyPath(AVPlayerItem.isPlaybackBufferFull):
            _isBuffering = false
        default:
            return
        }
    }
    
    @objc func playerItemDidReachEnd(_ notification: NSNotification) {
        let replay = delegate?.mediaPlayer(self, playableItemDidPlaytoEndTime: _currentPlayableItem) ?? false
        
        if replay {
            seekTo(time: 0)
            play()
        }
    }
}

// MARK: MediaStreamControl
extension FacadeAVPlayer : MediaStreamControl {
    
    private func setupPlayableMetadata(from asset:AVAsset) {
        
        _currentPlayableItem?.availableAudios.removeAll()
        _currentPlayableItem?.availableVideos.removeAll()
        _currentPlayableItem?.availableSubtitles.removeAll()
        
        let audioSelectionGrp = asset.mediaSelectionGroup(forMediaCharacteristic: .audible)
        for option in audioSelectionGrp?.options ?? [] {
            let ast = AVMediaAudioStream.init(withRawMetaData: option)
            if option == audioSelectionGrp?.defaultOption {
                ast.isSelected = true
            }
            _currentPlayableItem?.availableAudios.append(ast)
        }
        
        let videoSelectionGrp = asset.mediaSelectionGroup(forMediaCharacteristic: .visual)
        for option in videoSelectionGrp?.options ?? [] {
            let vst = AVMediaVideoStream.init(withRawMetaData: option)
            if option == audioSelectionGrp?.defaultOption {
                vst.isSelected = true
            }
            _currentPlayableItem?.availableVideos.append(vst)
        }
        
        let ccSelectionGrp = asset.mediaSelectionGroup(forMediaCharacteristic: .legible)
        for option in ccSelectionGrp?.options ?? [] {
            let ccst = AVMediaSubtitleStream.init(withRawMetaData: option)
            if option == audioSelectionGrp?.defaultOption {
                ccst.isSelected = true
            }
            _currentPlayableItem?.availableSubtitles.append(ccst)
        }
    }
    
    public func updateAudioStreams() {
        guard var playable = _currentPlayableItem else {
            return
        }
        let streams = playable.availableAudios
        if let audioSelectionGrp = avPlayer.currentItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .audible) {
            for i in 0..<streams.count {
                let stream = streams[i]
                if stream.isSelected {
                    avPlayer.currentItem?.select(stream.rawMediaStream as? AVMediaSelectionOption, in: audioSelectionGrp)
                    return
                }
            }
        }
    }
    
    public func updateVideoStreams() {
        guard var playable = _currentPlayableItem else {
            return
        }
        let streams = playable.availableVideos
        if let videoSelectionGrp = avPlayer.currentItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .visual) {
            for i in 0..<streams.count {
                let stream = streams[i]
                if stream.isSelected {
                    avPlayer.currentItem?.select(stream.rawMediaStream as? AVMediaSelectionOption, in: videoSelectionGrp)
                    return
                }
            }
        }
    }
    
    public func updateSubtitleStreams() {
        guard var playable = _currentPlayableItem else {
            return
        }
        let streams = playable.availableSubtitles
        if let ccSelectionGrp = avPlayer.currentItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .legible) {
            
            if streams.count == 0 {
                avPlayer.currentItem?.select(nil, in: ccSelectionGrp)
                return
            }
            
            let hasSelected = streams.contains(where: { (st) -> Bool in
                return st.isSelected
            })
            
            if !hasSelected {
                avPlayer.currentItem?.select(nil, in: ccSelectionGrp)
            }
            else {
                for i in 0..<streams.count {
                    let stream = streams[i]
                    if stream.isSelected {
                        avPlayer.currentItem?.select(stream.rawMediaStream as? AVMediaSelectionOption, in: ccSelectionGrp)
                        return
                    }
                }
            }
        }
    }
    
}

































