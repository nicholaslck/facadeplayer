//
//  FairPlayResourceProvider.swift
//  TFPlayer
//
//  Created by Nicholas LAU on 13/8/2018.
//

import Foundation

/// playable should implement this protocol if content is DRM protected by FairPlay
public protocol FairPlayResourceProvider : Playable {
    
    var fpsCertificateURL: String { get }
    
    var fpsServerURL: String { get }
    
    var drmToken: String { get }
    
}
