//
//  PlayerDelegate.swift
//  Player
//
//  Created by Nicholas LAU on 16/7/2018.
//

import Foundation

public protocol FacadePlayerDelegate : AnyObject {
    
    /// delegate to get latest playback status of current item.
    func mediaPlayer(_ player: FacadePlayer, playableItem: Playable?, playbackStatusDidChange toIsPlaying: Bool)
    
    /// delegate to get current buffering status.
    func mediaPlayer(_ player: FacadePlayer, playableItem: Playable?, bufferingStatusDidChange toIsBuffering: Bool)
    
    /// delegate to get current time and duration of current item. This delegate will be called every time when current time is updated.
    func mediaPlayer(_ player: FacadePlayer, playableItem: Playable?, currentTimeDidUpdate currentTime: TimeInterval, duration: TimeInterval)
    
    /// This delegate will be called once the current item's metadata is ready. You should etreive playable metadata (e.g. audioStreams) in this delegate.
    func mediaPlayer(_ player: FacadePlayer, playableItemMetadataDidReady playableItem: Playable)
    
    /// This delegate will be called only when metadata is failed to ready or encounter error.
    func mediaPlayer(_ player: FacadePlayer, playableItem: Playable?, metadataError: Error?)
    
    /**
     The delegate will be called when current item played to the end.
        - Returns: true if current item needs to replay from the beginning, else false
     */
    func mediaPlayer(_ player: FacadePlayer, playableItemDidPlaytoEndTime: Playable?) -> Bool
}
