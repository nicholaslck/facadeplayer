//
//  Playable.swift
//  Player
//
//  Created by Nicholas LAU on 11/7/2018.
//

import UIKit

public protocol PlayableMetadata {
    
    var url: URL? { get set }
    
    var duration: TimeInterval { get set }
    
    var availableVideos: [MediaVideoStream] { get set }
    
    var availableAudios: [MediaAudioStream] { get set }
    
    var availableSubtitles: [MediaSubtitleStream] { get set }
    
}

public protocol Playable: PlayableMetadata {
    
    var currentTime: TimeInterval { get set }
    
    /**
     This method only set internal media stream selected or not.
     To update stream into player, run Player
     
     -func updateAudioStreams()
     
     -func updateVideoStreams(),
     
     -func updateSubtitleStreams()
     
     respectively.
     */
    func selectMediaStream(index: Int, onStreamList streams: [MediaStream]) -> Bool
}

public extension Playable {
    
    public func selectMediaStream(index: Int, onStreamList streams: [MediaStream]) -> Bool {
        
        if index >= streams.count || index < 0 {
//            print("stream index \(index) invalid.")
            return false
        }
        
        for i in 0..<streams.count {
            var st = streams[i]
            st.isSelected = i == index ? true : false
        }
        
        return true
    }
    
}

public extension Playable {
    
    var selectedVideoIndex: Int? {
        return availableVideos.index { (stream) -> Bool in
            return stream.isSelected
        }
    }
    
    var selectedAudioIndex: Int? {
        return availableAudios.index(where: { (stream) -> Bool in
            return stream.isSelected
        })
    }
    
    var selectedSubtitleIndex: Int? {
        return availableSubtitles.index(where: { (stream) -> Bool in
            return stream.isSelected
        })
    }
    
}
