//
//  TFMediaStream.swift
//  Player
//
//  Created by Nicholas LAU on 12/7/2018.
//

import Foundation

public enum MediaStreamType {
    
    case unknown
    
    case audio
    
    case video
    
    case subtitle
    
}

public protocol MediaStream {
    
    var rawMediaStream: Any! { get }
    
    var mediaStreamType: MediaStreamType { get }
    
    var isSelected: Bool { get set }
    
    var name: String { get }
    
    var locale: String? { get }
    
}

public protocol MediaVideoStream : MediaStream {
    
    var bitRate : UInt { get }
    
    var width: UInt { get }
    
    var height: UInt { get }
    
}

public protocol MediaAudioStream : MediaStream {
    
    var bitRate : UInt { get }
    
}

public protocol MediaSubtitleStream : MediaStream {
}
